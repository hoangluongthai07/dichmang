﻿using System;

int[] arr = {1, 2, 3, 4, 5};

// In mảng ban đầu
int n = arr.Length;
for(int i = 0; i < n; i++) {
  Console.Write(arr[i] + " "); 
}

Console.WriteLine();

// Dịch chuyển mảng 
int temp = arr[0];
for(int i = 0; i < n - 1; i++) {
  arr[i] = arr[i + 1];
}
arr[n-1] = temp;

// In mảng sau khi dịch
for(int i = 0; i < n; i++) {
  Console.Write(arr[i] + " ");  
}

Console.WriteLine();